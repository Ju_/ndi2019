from .app import db, login_manager
import yaml
import os.path
# from flask_login import UserMixin

class Utilisateur(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    nom = db.Column(db.String(100))
    prenom = db.Column(db.String(100))
    sexe = db.Column(db.String(100))
    dateNaissance = db.Column(db.Date)
    email = db.Column(db.String(100))
    mdp = db.Column(db.String(500))
    estModo = db.Column(db.Boolean())
    img = db.Column(db.LargeBinary)

    def __repr__(self):
        return "<Utilisateur (%d) %s %s>" %(self.id, self.nom, self.prenom)

class Categorie(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))

    def __repr__(self):
        return "<Categorie (%d) %s>" %(self.id, self.nom)

class Sujet(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    titre = db.Column(db.String(100))
    categorie = db.relationship("Categorie", backref=db.backref("sujets", lazy="dynamic"))
    categorie_id = db.Column(db.Integer, db.ForeignKey(Categorie.id))

    def __repr__(self):
        return "<Sujet (%d) %s>" %(self.id, self.titre)

class Message(db.Model):
    num = db.Column(db.Integer, primary_key=True, nullable=False)
    contenu = db.Column(db.Text)
    date = db.Column(db.Date)
    utilisateur = db.relationship("Utilisateur", backref=db.backref("messages", lazy="dynamic"))
    sujet = db.relationship("Sujet", backref=db.backref("messages", lazy="dynamic"))
    utilisateur_id = db.Column(db.Integer, db.ForeignKey(Utilisateur.id), primary_key=True, nullable=False)
    sujet_id = db.Column(db.Integer, db.ForeignKey(Sujet.id), primary_key=True, nullable=False)

    def __repr__(self):
        return "<Message (%d)>" %(self.id)

class Universite(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    nom = db.Column(db.String(100))

    def __repr__(self):
        return "<Universite (%d) %s>" %(self.id, self.nom)

class Cursus(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    intitule = db.Column(db.String(100))
    universite = db.relationship("Universite", backref=db.backref("cursus", lazy="dynamic"))
    universite_id = db.Column(db.Integer, db.ForeignKey(Universite.id))

    def __repr__(self):
        return "<Cursus (%d) %s>" %(self.id, self.intitule)

class Parcours(db.Model):
    utilisateur_id = db.Column(db.Integer, db.ForeignKey(Utilisateur.id), primary_key=True, nullable=False)
    cursus_id = db.Column(db.Integer, db.ForeignKey(Cursus.id), primary_key=True, nullable=False)
    utilisateur = db.relationship("Utilisateur", backref=db.backref("parcours", lazy="dynamic"))
    cursus = db.relationship("Cursus", backref=db.backref("parcours", lazy="dynamic"))
    enCours = db.Column(db.Boolean(), primary_key=True, nullable=False)
    dateDebut = db.Column(db.Date, primary_key=True, nullable=False)
    dateFin = db.Column(db.Date, primary_key=True, nullable=False)

    def __repr__(self):
        return "<Sujet (%d) %s>" %(self.id, self.titre)

