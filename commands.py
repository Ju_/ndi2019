import click
import datetime
from .app import app, db
from .models import *

@app.cli.command()
@click.argument("filename")
def loaddb(filename):
    """
    Creates the tables and populates them with data
    """

    # creation de toutes les tables 
    db.create_all()

    # chargement de notre jeu de donnees de base
    import yaml
    dico = yaml.load(open(filename))


    # premiere passe : creation de tous les auteurs
    users = set()
    univs = set()
    cursuses = set()
    categories = set()
    sujet = set()

    index_univ = 0
    index_cursus = 0
    index_categorie = 0
    index_sujet = 0
    index_message = 0

    for i in dico:
        infos = i["infos"]
        if infos == "Parcours":

            user = (i["idUser"], i["prenom"], i["nom"])
            users.add(user)
            user_instance = db_fast_add_user(db, user)

            univ = (i["univ"])
            if univ not in univs:
                univs.add(univ)
                index_univ += 1
                univ_instance = db_fast_add_univ(db, index_univ, univ)

            cursus = (i["cursus"], i["univ"])
            if cursus not in cursuses:
                cursuses.add(cursus)
                index_cursus += 1
                univ_instance = Universite.query.get_or_404(index_univ)
                print(univ_instance)
                cursus_instance = db_fast_add_cursus(db, index_cursus, cursus[0], univ_instance)

            user_instance = Utilisateur.query.get_or_404(i["idUser"])
            db_fast_add_parcours(db, user_instance, cursus_instance)

        if infos == "categories":
            index_categorie += 1
            nomCat = i["categorie"]
            db_fast_add_categorie(db, index_categorie, nomCat)

        if infos == "sujet":
            index_sujet += 1
            titreSujet = i["sujet"]
            id = index_sujet % index_categorie
            categorie = Categorie.query.get_or_404(id)
            db_fast_add_sujet(db, index_sujet, titreSujet, categorie)

        if infos == "message":
            index_message += 1
            
            num = index_message % 2
            contenu = i["contenu"]
            user = Utilisateur.query.get_or_404(i["idUser"])
            sujet = index_message.query.get_or_404(index_message%index_sujet)
            db_fast_add_message(db, num, contenu, user, sujet)


    db.session.commit()


def db_fast_add_user(db, user_tuple):
    to_add = Utilisateur(
        id = user_tuple[0],
        nom = user_tuple[2],
        prenom = user_tuple[1],
        sexe = "Non renseigné",
        dateNaissance = datetime.date.today(),
        email = None,
        mdp = None,
        estModo = False,
        img = None
    )
    db.session.add(to_add)

def db_fast_add_univ(db, id, univ_name):
    to_add = Universite(
        id = id,
        nom = univ_name
    )
    db.session.add(to_add)
    return to_add

def db_fast_add_cursus(db, id, cursus_intitule, univ):
    to_add = Cursus(
        id = id,
        intitule = cursus_intitule,
        universite_id = univ.id
    )
    db.session.add(to_add)
    return to_add

def db_fast_add_parcours(db, user, cursus):
    to_add = Parcours(
        utilisateur_id = user.id,
        cursus_id = cursus.id,
        enCours = True,
        dateDebut = datetime.date.today(),
        dateFin = datetime.date.today(),
    )
    db.session.add(to_add)
    return to_add


def db_fast_add_categorie(db, id, nomCat):
    to_add = Categorie(
        id = id,
        nom = nomCat
    )
    db.session.add(to_add)

def db_fast_add_sujet(db, id, titreSujet, categorie):
    to_add = Sujet(
        id = id,
        titre = titreSujet,
        categorie = categorie
    )
    db.session.add(to_add)

def db_fast_add_message(db, num, contenu, utilisateur, sujet):
    to_add = Sujet(
        num = num,
        contenu = contenu,
        date = datetime.date.today(),
        utilisateur = utilisateur,
        sujet = sujet
    )
    db.session.add(to_add)

@app.cli.command()
def syncdb():
    """
    Creates all missing tables.
    """
    db.create_all()
