from .app import app
from flask import render_template, url_for, redirect
from flask_login import logout_user, login_user, current_user
# from flask_wtf import FlaskForm
# from wtforms import StringField, HiddenField, validators, PasswordField
# from hashlib import sha256

CAT = ["Santé", "Logement", "Bourse", "Emplois", "Etudes", "Bons plans"]
POST = [
    {
        "title" : "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ?",
        'author' : "XXX",
        "date" : "23 Nov",
        "categorie" : "Santé"
    },
    {
        "title" : "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ?",
        'author' : "XXX",
        "date" : "23 Nov",
        "categorie" : "Santé"
    }
]
USER = None
MESSAGES_BOT = ["Bonjour ! Je suis le chatbot. Besoin de renseignements ?", "test"]

@app.route("/")
def index():
    # if not current_user.is_authenticated:
    #     return render_template(
    #     "visitor_home.html"
    #     )
    # else:
        return render_template(
        "chatbot.html",
        title="Bienvenue !",
        msg=enumerate(MESSAGES_BOT)
        )

@app.route("/forum")
def forum():
    return render_template(
        "forum.html",
        title="Forum",
        cat=CAT,
        posts=POST,
        user=USER
    )

@app.route("/forum/<idPost>")
def post(idPost):
    return ""

@app.route("/connexion")
def connexion():
    return render_template("visitor_home.html")

@app.route("/deconnexion")
def deconnexion():
    logout_user()
    return redirect(url_for('index'))

@app.route("/inscription")
def inscription():
    return ""

@app.route("/renseignements")
def renseignements():
    return ""

@app.route("/profil")
def profil():
    return ""

@app.errorhandler(404)
def page_not_found(e):
    return render_template(
        '404.html'
    ),404