from flask import Flask
# from flask_bootstrap import Bootstrap

app = Flask(__name__)
# Bootstrap(app)

import os.path

def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__), p
        )
    )

from flask_sqlalchemy import SQLAlchemy
app.config["SQLALCHEMY_DATABASE_URI"] = (
    "sqlite:///"+mkpath("../ndi2019.db")
)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
db = SQLAlchemy(app)


from flask_login import LoginManager
app.config["SECRET_KEY"] = "cee66fd4-60e0-4ff0-96f5-8102964e87c0"
login_manager = LoginManager(app)